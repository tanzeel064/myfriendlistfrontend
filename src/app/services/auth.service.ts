import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  url: any = 'http://localhost:3000/api'
  constructor(public http: HttpClient) { }

  register(data: any) {
    let body = JSON.stringify(data);
    const header = {
      'Content-Type': 'application/json'
    };

    const options = {
      headers: header
    };
    return new Promise((resolve, reject) => {
      this.http.post(this.url + '/register', data, options).subscribe(res => {
        resolve(res);
      }, (err) => { 
        reject(err) })
    })
  }

  login(data: any) {
    let body = JSON.stringify(data);
    const header = {
      'Content-Type': 'application/json'
    };

    const options = {
      headers: header
    };
    return new Promise((resolve, reject) => {
      this.http.post(this.url + '/login', data, options).subscribe(res => {
        resolve(res);
      }, (err) => { 
        reject(err) })
    })
  }

  getAllUsers() {
    let token = localStorage.getItem('userToken');
    const header = {
      'x-access-token': token
    };

    const options = {
      headers: header
    };
    return new Promise((resolve, reject) => {
      this.http.get(this.url + '/getAllUsers', options).subscribe(res => {
        resolve(res);
      }, (err) => { 
        reject(err) })
    })
  }
  
  addFriend(friend: any) {
    let token = localStorage.getItem('userToken');
    const header = {
      'x-access-token': token
    };

    const options = {
      headers: header
    };
    let data = JSON.parse(localStorage.getItem('userData'))
    if(data) {
    let index = data.friendsIds.findIndex(k => k === friend._id)
    if(index<0)data.friendsIds.push(friend._id);
    return new Promise((resolve, reject) => {
      this.http.put(this.url + '/addFriend/'+data._id, data, options).subscribe(res => {
        resolve(res);
      }, (err) => { 
        reject(err) })
    });
  }
  }
  getAllFriends() {
    let token = localStorage.getItem('userToken');
    const header = {
      'x-access-token': token
    };

    const options = {
      headers: header
    };
    let data = JSON.parse(localStorage.getItem('userData'))
    if(data) {
    return new Promise((resolve, reject) => {
      this.http.get(this.url + '/getAllFriends/'+data._id, options).subscribe(res => {
        resolve(res);
      }, (err) => { 
        reject(err) })
    });
  }
  }
  removeFriend(friend: any) {
    let token = localStorage.getItem('userToken');
    const header = {
      'x-access-token': token
    };

    const options = {
      headers: header
    };
    let data = JSON.parse(localStorage.getItem('userData'))
    if(data) {
    let index = data.friendsIds.findIndex(k => k === friend._id)
    if(index>=0){data.friendsIds.splice(index, 1)};
    debugger
    return new Promise((resolve, reject) => {
      this.http.put(this.url + '/addFriend/'+data._id, data, options).subscribe(res => {
        resolve(res);
      }, (err) => { 
        reject(err) })
    });
  }
  }
}
