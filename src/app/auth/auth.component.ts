import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  showLogin: boolean = true;
  userData: any = { name: '', email: '', password: '', friendsIds: [''] };
  loginData: any = { email: '', password: '', }
  btnText = 'Do not have an account? Register';

  constructor(private auth: AuthService, public router: Router, public toaster: ToastrService) {

  }

  ngOnInit() {

  }

  loginRegisterSwitch(action: any) {
    if (action === 'login') {
      this.showLogin = false;
      this.btnText = 'Already have an account! Login'
    } else if (action === "register") {
      this.showLogin = true;
      this.btnText = 'Do not have an account? Register'
    }
  }
  register() {
    if (this.userData.name, this.userData.email, this.userData.password) {
      this.auth.register(this.userData).then(data => {
        this.showLogin = true;
      }).catch(err => {
        console.log(err);
      })
    }
  }

  login() {
    if (this.loginData.email, this.loginData.password) {
      this.auth.login(this.loginData).then(data => {
        this.toaster.success(data['message'])
        localStorage.setItem("userToken", data['token']);
        localStorage.setItem("userData", JSON.stringify(data['user']));
        this.router.navigate(['/home']);
      }).catch(err => {
        this.toaster.error(err['error'].message)
        console.log(err);
      })
    }
  }
}
