import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  showAllUsers: boolean = true;
  btnText: string = 'Show All Friends';
  usersList: any[] = [];
  friendsList: any[] = [];

  constructor(public auth: AuthService, public router: Router) { }

  ngOnInit() {
    this.getAllUsers();
  }

  getAllUsers() {
    this.auth.getAllUsers().then(data => {
     this.usersList = data['data'];
    }).catch(err => {
      console.log(err)
    })
  }
  friendUserSwitch(action: any) {
    if (action === 'friends') {
      this.showAllUsers = false;
      this.btnText = 'Show All Users'
    } else if (action === "users") {
      this.showAllUsers = true;
      this.btnText = 'Show All Friends'
    }
  }
  addFriend(user: any) {
    this.auth.addFriend(user).then(data => {
      localStorage.setItem('userData', JSON.stringify(data['data']));
    }).catch(err => {
      console.log(err)
    })
  }
  getAllFriends() {
    this.auth.getAllFriends().then(data => {
      this.friendsList = data['data']
    }).catch(err => {
      console.log(err)
    })
  }
  logout() {
    localStorage.clear();
    this.router.navigate(['/auth']);
  }
  removeFriend(user: any) {
    this.auth.removeFriend(user).then(data => {
      localStorage.setItem('userData', JSON.stringify(data['data']));
      this.getAllFriends();
    }).catch(err => {
      console.log(err)
    })
  }
}
